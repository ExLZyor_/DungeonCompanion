﻿using Assets.Scripts.MainCode.Managers.DebugManager;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.Globals
{
    public class DebugUtils
    {
        private static DebugManager GetDebugManager()
        {
            return DebugManager.Instance;
        }

        public static bool IsFlagActivated(string _flag)
        {
            var debugMgr = GetDebugManager();
            if(debugMgr)
            {
                return debugMgr.IsFlagOn(_flag);
            }
            return false;
        }
    }
}