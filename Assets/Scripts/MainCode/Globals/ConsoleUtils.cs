﻿using Assets.Scripts.MainCode.Managers.ConsoleManager;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.Globals
{
    public class ConsoleUtils : ScriptableObject
    {
        private static ConsoleManager GetConsoleManager()
        {
            return ConsoleManager.Instance;
        }

        public static void AddInfoMessage(string _message)
        {
            var console = GetConsoleManager();
            if(console)
            {
                console.AddInfoMessage(_message);
            }
        }
        public static void AddWarningMessage(string _message)
        {
            var console = GetConsoleManager();
            if (console)
            {
                console.AddWarningMessage(_message);
            }
        }
        public static void AddErrorMessage(string _message)
        {
            var console = GetConsoleManager();
            if (console)
            {
                console.AddErrorMessage(_message);
            }
        }
    }
}