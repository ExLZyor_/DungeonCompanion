﻿using Assets.Scripts.MainCode.Managers;
using Assets.Scripts.MainCode.Managers.LoginManager;
using Assets.Scripts.MainCode.Managers.PlayFabManager;
using Assets.Scripts.MainCode.Managers.ProfileManager;
using Assets.Scripts.MainCode.PlayFab.Services.Login;
using Assets.Scripts.MainCode.PlayFab.Services.Profile;
using PlayFab;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.Globals
{
    public class ManagerUtils
    {
        public static PlayFabManager GetPlayFabManager()
        {
            return PlayFabManager.Instance;
        }
        public static PlayFabAuthenticationContext GetPlayFabAuthenticationContext()
        {
            var playfabMgr = GetPlayFabManager();
            if (playfabMgr)
            {
                return playfabMgr.AuthenticationContext;
            }
            return null;
        }
        public static PlayFabLoginService GetPlayFabLoginService()
        {
            var playfabMgr = GetPlayFabManager();
            if (playfabMgr)
                return playfabMgr.LoginService;

            return null;
        }
        public static PlayFabProfileService GetPlayFabProfileService()
        {
            var playfabMgr = GetPlayFabManager();
            if (playfabMgr)
                return playfabMgr.ProfileService;

            return null;
        }

        public static LoginManager GetLoginManager()
        {
            return LoginManager.Instance;
        }

        public static CommandManager GetCommandManager()
        {
            return CommandManager.Instance;
        }

        public static ProfileManager GetProfileManager()
        {
            return ProfileManager.Instance;
        }

        public static string GetProfilePlayFabId()
        {
            var profileMgr = GetProfileManager();
            if(profileMgr)
            {
                return profileMgr.GetPlayFabId();
            }
            return "";
        }
    }
}