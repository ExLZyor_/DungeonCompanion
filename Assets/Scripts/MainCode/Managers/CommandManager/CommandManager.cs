﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MainCode.Managers
{
    public class CommandManager : MonoBehaviour
    {
        public delegate string Command(List<string> _args);

        private static CommandManager m_instance;
        public static CommandManager Instance { get => m_instance; }

        private Dictionary<string, Command> m_commands;

        private void Awake()
        {
            if(m_instance == null)
            {
                m_instance = GetComponent<CommandManager>();
            }
            var comparer = StringComparer.OrdinalIgnoreCase;
            m_commands = new Dictionary<string, Command>(comparer);
            DontDestroyOnLoad(this.gameObject);
        }

        public void RegisterCommand(string _strCommand, Command _command)
        {
            m_commands.Add(_strCommand, _command);
        }

        public string ActivateCommand(string _strCommand)
        {
            return ActivateCommand(_strCommand, new List<string>());
        }

        public string ActivateCommand(string _strCommand, List<string> _args)
        {
            if(m_commands.TryGetValue(_strCommand, out Command _command))
            {
                return _command(_args);
            }
            return string.Format("Unknown command : {0}", _strCommand);
        }
    }
}