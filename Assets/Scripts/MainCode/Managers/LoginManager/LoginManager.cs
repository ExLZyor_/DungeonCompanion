﻿using Assets.Scripts.MainCode.PlayFab.Client.Login;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MainCode.Managers.LoginManager
{
    public class LoginManager : MonoBehaviour
    {
        private static LoginManager m_instance;
        public static LoginManager Instance { get => m_instance; }

        private PlayFabLoginWithCustomIdRequest m_loginWithCustomIdRequest;


        // Use this for initialization
        void Start()
        {
            var commandManager = Globals.ManagerUtils.GetCommandManager();
            if(commandManager)
            {
                commandManager.RegisterCommand("DebugLoginWithCustomId", _DebugLoginWithCustomId);
            }
        }

        private void Awake()
        {
            if(m_instance == null)
            {
                m_instance = GetComponent<LoginManager>();
                m_loginWithCustomIdRequest = new PlayFabLoginWithCustomIdRequest();
                DontDestroyOnLoad(this.gameObject);
            }
        }

        // Update is called once per frame
        void Update()
        {
            _ProcessLoginWithCustomId();
        }


        // Requests
        private void _ProcessLoginWithCustomId()
        {
            if(m_loginWithCustomIdRequest.IsCompleted())
            {
                if(m_loginWithCustomIdRequest.IsSuceeded())
                {
                    var profileMgr = Globals.ManagerUtils.GetProfileManager();
                    if(profileMgr)
                    {
                        profileMgr.SetPlayFabId(m_loginWithCustomIdRequest.PlayFabId);
                    }

                    var playFabMgr = Globals.ManagerUtils.GetPlayFabManager();
                    if(playFabMgr)
                    {
                        playFabMgr.SetAuthenticationContext(m_loginWithCustomIdRequest.AuthenticationContext);
                    }
                }
                else
                {
                    Globals.ConsoleUtils.AddErrorMessage("[_ProcessLoginWithCustomId] Login failed !");
                }
                m_loginWithCustomIdRequest.Reset();
            }
        }

        private void _StartLoginWithCustomId(string _strCustomId, bool _bCreateAccount)
        {
            if (!m_loginWithCustomIdRequest.IsStarted())
            {
                var loginService = Globals.ManagerUtils.GetPlayFabLoginService();

                m_loginWithCustomIdRequest = loginService.RequestLoginWithCustomId(_strCustomId, _bCreateAccount);
            }
        }

        private string _DebugLoginWithCustomId(List<string> _args)
        {
            if(_args.Count == 0)
            {
                string strNewId = LoginWithNewCustomId();
                return string.Format("Starting login process with id {0}", strNewId);
            }
            return "_DebugLoginWithCustomId requires no argument.";
        }

        public string LoginWithNewCustomId()
        {
            string strMachineId = string.Format("{0}-{1}", System.Environment.MachineName, System.Environment.UserName);

            int iHash = Mathf.Abs(strMachineId.GetHashCode() % 999999);
            _StartLoginWithCustomId(iHash.ToString(), true);
            return iHash.ToString();
        }

        public void LoginWithKnownCustomId(string _strId)
        {
            _StartLoginWithCustomId(_strId, false);
        }

    }
}