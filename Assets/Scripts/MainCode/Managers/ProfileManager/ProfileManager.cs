﻿using Assets.Scripts.MainCode.PlayFab.Client.Profile;
using Assets.Scripts.MainCode.User;
using UnityEngine;

namespace Assets.Scripts.MainCode.Managers.ProfileManager
{
    public class ProfileManager : MonoBehaviour
    {
        private static ProfileManager m_instance;
        public static ProfileManager Instance { get => m_instance; }
        private PlayFabGetPlayerTagsRequest m_getPlayerTagsRequest;

        private OnlineProfile m_onlineProfile;

        // Use this for initialization
        void Start()
        {

        }

        private void Awake()
        {
            if (m_instance == null)
            {
                m_instance = GetComponent<ProfileManager>();
                m_getPlayerTagsRequest = new PlayFabGetPlayerTagsRequest();
                m_onlineProfile = new OnlineProfile();
                DontDestroyOnLoad(this.gameObject);
            }
        }

        // Update is called once per frame
        void Update()
        {
            _ProcessGetPlayerTags();
        }

        public string GetPlayFabId()
        {
            return m_onlineProfile.PlayFabId;
        }
        public void SetPlayFabId(string _strId)
        {
            m_onlineProfile.SetPlayFabId(_strId);
        }

        // Requests
        private void _ProcessGetPlayerTags()
        {
            if (m_getPlayerTagsRequest.IsCompleted())
            {
                if (m_getPlayerTagsRequest.IsSuceeded())
                {
                    m_onlineProfile.SetTags(m_getPlayerTagsRequest.Tags);
                }
                else
                {
                    Globals.ConsoleUtils.AddErrorMessage("[_ProcessGetPlayerTags] Request failed !");
                }
                m_getPlayerTagsRequest.Reset();
            }
        }

        public void StartGetPlayerTags()
        {
            if (!m_getPlayerTagsRequest.IsStarted())
            {
                var profileService = Globals.ManagerUtils.GetPlayFabProfileService();

                m_getPlayerTagsRequest = profileService.RequestGetPlayerTags(Globals.ManagerUtils.GetProfilePlayFabId());
            }
        }
    }
}