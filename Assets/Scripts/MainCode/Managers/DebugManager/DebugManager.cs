﻿using Assets.Scripts.MainCode.Globals;
using ImGuiNET;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MainCode.Managers.DebugManager
{
    public class DebugManager : MonoBehaviour
    {
        private static DebugManager m_instance;
        public static DebugManager Instance { get => m_instance; }
        Dictionary<string, bool> m_hDebugFlags;

        private Rect m_currentDebugWindowRect;
        private Vector2 m_scrollPosition;
        // Use this for initialization
        void Start()
        {
            var commandManager = Globals.ManagerUtils.GetCommandManager();
            if (commandManager)
            {
                commandManager.RegisterCommand("Active", Active);
                commandManager.RegisterCommand("Deactive", Deactive);
            }
        }

        private void Awake()
        {
            if(m_instance == null)
            {
                m_instance = GetComponent<DebugManager>();
            }

            var comparer = StringComparer.OrdinalIgnoreCase;
            m_hDebugFlags = new Dictionary<string, bool>(comparer);
            m_currentDebugWindowRect = new Rect(0, 0, Screen.width * 0.5f, Screen.height * 0.8f);
            m_scrollPosition = Vector2.zero;
            DontDestroyOnLoad(this.gameObject);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnEnable()
        {
            ImGuiUn.Layout += DrawFlagWindow;
        }

        private void OnDisable()
        {
            ImGuiUn.Layout -= DrawFlagWindow;
        }

        void DrawFlagWindow()
        {
            if (!DebugUtils.IsFlagActivated("DrawDebugFlags"))
            {
                return;
            }

            if (ImGui.Begin("Flags"))
            {
                foreach (var strFlag in m_hDebugFlags.Keys)
                {
                    bool val = m_hDebugFlags[strFlag];
                    ImGui.PushStyleColor(0, val ? Color.green : Color.red);
                    ImGui.Text(strFlag);
                    ImGui.PopStyleColor();
                }
            }
            ImGui.End();


        }

        void DrawFlagWindow(int windowId)
        {
            GUI.DragWindow(new Rect(0, 0, 10000, 20));
            m_scrollPosition = GUI.BeginScrollView(new Rect(0, 0.0f, m_currentDebugWindowRect.size.x, m_currentDebugWindowRect.size.y - 20.0f), m_scrollPosition, new Rect(0, 0, m_currentDebugWindowRect.size.x, m_hDebugFlags.Count * 20.0f));

            var oldColor = GUI.color;
            int curIndex = 0;
            foreach (var strFlag in m_hDebugFlags.Keys)
            {
                bool val = m_hDebugFlags[strFlag];
                GUI.color = val ? Color.green : Color.red;
                GUI.TextField(new Rect(0, (1 + curIndex) * 20.0f, m_currentDebugWindowRect.size.x, 20.0f), string.Format("{0} : {1}", strFlag, val ? "true" : "false"));
                curIndex++;
            }
            GUI.EndScrollView();
        }

        public bool IsFlagOn(string _flag)
        {
            if(m_hDebugFlags.TryGetValue(_flag, out bool v))
            {
                return v;
            }
            return false;
        }

        private string Active(List<string> _args)
        {
            if(_args.Count == 1)
            {
                var flag = _args[0];
                if (m_hDebugFlags.ContainsKey(flag))
                {
                    m_hDebugFlags[flag] = !m_hDebugFlags[flag];
                }
                else
                {
                    m_hDebugFlags.Add(flag, true);
                }
                return string.Format("'{0}' is now {1}", flag, m_hDebugFlags[flag] ? "on" : "off");
            }

            return "Active requires exactly one argument.";
        }

        private string Deactive(List<string> _args)
        {
            if (_args.Count == 1)
            {
                var flag = _args[0];
                if (m_hDebugFlags.ContainsKey(flag))
                {
                    m_hDebugFlags[flag] = false;
                    return string.Format("'{0}' is now off", flag);
                }
                return string.Format("Unset flag '{0}'", flag);
            }

            return "Deactive requires exactly one argument.";
        }
    }
}