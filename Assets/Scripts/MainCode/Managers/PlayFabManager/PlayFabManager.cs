﻿using Assets.Scripts.MainCode.Globals;
using Assets.Scripts.MainCode.PlayFab.Model;
using Assets.Scripts.MainCode.PlayFab.Services.Login;
using Assets.Scripts.MainCode.PlayFab.Services.Profile;
using ImGuiNET;
using PlayFab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MainCode.Managers.PlayFabManager
{
    public class PlayFabManager : MonoBehaviour
    {
        private static PlayFabManager m_instance;
        public static PlayFabManager Instance { get => m_instance; }


        private PlayFabAuthenticationContext m_authenticationContext;
        public PlayFabAuthenticationContext AuthenticationContext { get => m_authenticationContext; }
        public void SetAuthenticationContext(PlayFabAuthenticationContext _context)
        {
            m_authenticationContext = _context;
        }


        private List<PlayFabService> m_services;
        private PlayFabLoginService m_loginService; // Here malke a service for steam also
        public PlayFabLoginService LoginService { get => m_loginService; }

        private PlayFabProfileService m_profileService;
        public PlayFabProfileService ProfileService { get => m_profileService; }



        private int m_iCurrentDebugIndex;

        // Use this for initialization
        void Start()
        {
#if ENABLE_PLAYFABSERVER_API
            var commandMgr = Globals.ManagerUtils.GetCommandManager();
            if(commandMgr)
            {
                commandMgr.RegisterCommand("SetSecretKey", _SetSecretKey);
            }
#endif
        }

        private void Awake()
        {
            if(m_instance == null)
            {
                m_instance = GetComponent<PlayFabManager>();
                if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
                {
                    PlayFabSettings.staticSettings.TitleId = Globals.PlayFabGlobals.TITLE_ID;
                    PlayFabSettings.DeveloperSecretKey = "";
                }

                m_authenticationContext = new PlayFabAuthenticationContext();

                m_services = new List<PlayFabService>();

                m_loginService = new PlayFabLoginService("PlayFabLoginService"); // here use the right type depending on config
                m_loginService.InitService();
                m_services.Add(m_loginService);

                m_profileService = new PlayFabProfileService("PlayFabProfileService");
                m_profileService.InitService();
                m_services.Add(m_profileService);

                m_iCurrentDebugIndex = 0;

                DontDestroyOnLoad(this.gameObject);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnEnable()
        {
            ImGuiUn.Layout += _DrawDebug;
        }

        private void OnDisable()
        {
            ImGuiUn.Layout -= _DrawDebug;
        }

        private void _DrawDebug()
        {
            if(!DebugUtils.IsFlagActivated("DrawDebugPlayFabServices"))
            {
                return;
            }

            if (ImGui.Begin("PlayFab Services"))
            {
                var windowSize = ImGui.GetWindowSize();
                var buttonSize = windowSize.x / 3.0f;
                if (ImGui.BeginChild("ServicesList", new Vector2(buttonSize, windowSize.y)))
                {
                    for (int i = 0; i < m_services.Count; ++i)
                    {
                        bool bChangeColor = (i == m_iCurrentDebugIndex);
                        if (bChangeColor)
                        {
                            ImGui.PushStyleColor(ImGuiCol.Button, new Vector4(0, 0, 255, 140));
                        }
                        if (ImGui.Button(m_services[i].DebugName, new Vector2(buttonSize, ImGui.GetFrameHeight())))
                        {
                            m_iCurrentDebugIndex = i;
                        }
                        if (bChangeColor)
                        {
                            ImGui.PopStyleColor();
                        }
                    }
                    ImGui.EndChild();
                }
                ImGui.SameLine();
                m_services[m_iCurrentDebugIndex].DrawDebug();
            }
            ImGui.End();
        }

#if ENABLE_PLAYFABSERVER_API
        private string _SetSecretKey(List<string> _args)
        {
            if(_args.Count == 1)
            {
                PlayFabSettings.DeveloperSecretKey = _args[0];
                return "SecretKey set.";
            }
            return "";
        }
    }
#endif
}