﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.Managers.ConsoleManager
{
    public class DisplayableMessage
    {
        public enum eLevel
        {
            Level_Info,
            Level_Warning,
            Level_Error
        }

        private eLevel m_eLevel;
        public eLevel Level;

        private string m_strBody;
        public string Body { get => m_strBody; }

        public DisplayableMessage(string _strBody, eLevel _eLevel)
        {
            m_strBody = _strBody;
            m_eLevel = _eLevel;
        }

        public Color GetColor()
        {
            switch(m_eLevel)
            {
                case eLevel.Level_Info:
                    return Color.white;
                case eLevel.Level_Warning:
                    return Color.yellow;
                case eLevel.Level_Error:
                    return Color.red;
            }

            return Color.white;
        }
    }
}