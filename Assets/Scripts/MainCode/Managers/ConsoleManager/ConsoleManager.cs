﻿using Assets.Scripts.MainCode.Globals;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ImGuiNET;

namespace Assets.Scripts.MainCode.Managers.ConsoleManager
{
    public class ConsoleFilter
    {
        public Dictionary<DisplayableMessage.eLevel, bool> m_filterLevels;

        public ConsoleFilter()
        {
            m_filterLevels = new Dictionary<DisplayableMessage.eLevel, bool>();
            m_filterLevels.Add(DisplayableMessage.eLevel.Level_Info, true);
            m_filterLevels.Add(DisplayableMessage.eLevel.Level_Warning, true);
            m_filterLevels.Add(DisplayableMessage.eLevel.Level_Error, true);
        }

        public bool FiltersMessage(DisplayableMessage _message)
        {
            return m_filterLevels[_message.Level];
        }

        public void SetFilterLevel(DisplayableMessage.eLevel _level, bool _bState)
        {
            m_filterLevels[_level] = _bState;
        }
        public bool GetFilterLevel(DisplayableMessage.eLevel _level)
        {
            return m_filterLevels[_level];
        }
    }


    public class ConsoleManager : MonoBehaviour
    {
        private static ConsoleManager m_instance;
        public static ConsoleManager Instance { get => m_instance; }


        private bool m_bShowGUI;
        private bool m_bFocusNextFrame;
        private bool m_bAutoScrollToBottom;
        private bool m_bScrollToBottomNextFrame;

        private string m_strCurrentCommand;
        private Rect m_currentConsoleRect;
        private Vector2 m_scrollPosition;

        private List<DisplayableMessage> m_messages;
        private List<DisplayableMessage> m_filteredMessages;

        private Queue<string> m_commandQueue;
        private int m_iCurrentQueueIndex;
        private const int MAX_QUEUE_SIZE = 8;

        private ConsoleFilter m_filter;


        // Use this for initialization
        void Start()
        {
            var commandManager = ManagerUtils.GetCommandManager();
            if(commandManager)
            {
                commandManager.RegisterCommand("AddInfoMessage", AddInfoMessage);
                commandManager.RegisterCommand("AddWarningMessage", AddWarningMessage);
                commandManager.RegisterCommand("AddErrorMessage", AddErrorMessage);
            }
        }

        private void Awake()
        {
            if(m_instance == null)
            {
                m_instance = GetComponent<ConsoleManager>();
            }

            m_bShowGUI = false;
            m_bFocusNextFrame = false;
            m_bScrollToBottomNextFrame = false;
            m_bAutoScrollToBottom = true;


            m_messages = new List<DisplayableMessage>();
            m_filteredMessages = new List<DisplayableMessage>();
            m_strCurrentCommand = "";

            m_commandQueue = new Queue<string>();
            m_iCurrentQueueIndex = 0;

            m_filter = new ConsoleFilter();
            m_currentConsoleRect = new Rect(0, Screen.height * 0.5f, Screen.width, Screen.height * 0.5f);
            m_scrollPosition = new Vector2(0.0f, 0.0f);

            DontDestroyOnLoad(this.gameObject);
        }

        private void OnEnable()
        {
            ImGuiUn.Layout += DrawConsole;
        }

        private void OnDisable()
        {
            ImGuiUn.Layout -= DrawConsole;
        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Quote))
            {
                ChangeGUIState();
            }
        }

        void DrawConsole()
        {
            if (!m_bShowGUI)
                return;

            if (ImGui.Begin("Console"))
            {
                var windowSize = ImGui.GetWindowSize();

                {
                    bool bInfo = m_filter.GetFilterLevel(DisplayableMessage.eLevel.Level_Info);
                    if (ImGui.Checkbox("Info", ref bInfo))
                    {
                        m_filter.SetFilterLevel(DisplayableMessage.eLevel.Level_Info, bInfo);
                        FilterMessages();
                    }
                    ImGui.SameLine();
                    bool bWarn = m_filter.GetFilterLevel(DisplayableMessage.eLevel.Level_Warning);
                    if (ImGui.Checkbox("Warning", ref bWarn))
                    {
                        m_filter.SetFilterLevel(DisplayableMessage.eLevel.Level_Warning, bWarn);
                        FilterMessages();
                    }
                    ImGui.SameLine();
                    bool bErr = m_filter.GetFilterLevel(DisplayableMessage.eLevel.Level_Error);
                    if (ImGui.Checkbox("Error", ref bErr))
                    {
                        m_filter.SetFilterLevel(DisplayableMessage.eLevel.Level_Error, bErr);
                        FilterMessages();
                    }
                    ImGui.SameLine();
                    ImGui.Checkbox("Automatically scroll to bottom", ref m_bAutoScrollToBottom);
                }



                if (ImGui.BeginChild("AllConsoleMessages", new Vector2(windowSize.x - ImGui.GetStyle().ItemInnerSpacing.x * 4, windowSize.y - 90), true))
                {
                    for (int i = 0; i < m_filteredMessages.Count; ++i)
                    {
                        Vector4 color = m_filteredMessages[i].GetColor();
                        ImGui.PushStyleColor(0, color);
                        ImGui.Text(m_filteredMessages[i].Body);
                        ImGui.PopStyleColor();
                    }
                    if (m_bScrollToBottomNextFrame)
                    {
                        m_bScrollToBottomNextFrame = false;
                        ImGui.SetScrollHereY();
                    }
                    ImGui.EndChild();
                }

                //ImGui.SetCursorPos(new Vector2(0, windowSize.y - ImGui.GetFrameHeight()));


                if (m_bFocusNextFrame)
                {
                    m_bFocusNextFrame = false;
                    ImGui.SetKeyboardFocusHere();
                }
                ImGui.SetNextItemWidth(windowSize.x);
                if (ImGui.InputText("", ref m_strCurrentCommand, 128, ImGuiInputTextFlags.EnterReturnsTrue))
                {
                    ActivateCommand();
                    m_bFocusNextFrame = true;
                }
            }
            ImGui.End();
        }

        public void ChangeGUIState()
        {
            m_bShowGUI = !m_bShowGUI;
            m_bFocusNextFrame = m_bShowGUI;
        }

        private void FilterMessages()
        {
           m_filteredMessages = m_messages.Where(m => m_filter.FiltersMessage(m)).ToList();
        }

        private void AddMessage(string _strBody, DisplayableMessage.eLevel _eLevel)
        {
            DisplayableMessage newMessage = new DisplayableMessage(_strBody, _eLevel);
            m_messages.Add(newMessage);

            if(m_filter.FiltersMessage(newMessage))
            {
                m_filteredMessages.Add(newMessage);
            }
        }

        private string AddInfoMessage(List<string> _args)
        {
            if(_args.Count == 1)
            {
                AddInfoMessage(_args[0]);
                return "";
            }
            return "AddInfoMessage requires exactly 1 argument";
        }
        public void AddInfoMessage(string _message)
        {
            if (!string.IsNullOrWhiteSpace(_message))
            {
                AddMessage(_message, DisplayableMessage.eLevel.Level_Info);
            }
        }
        private string AddWarningMessage(List<string> _args)
        {
            if (_args.Count == 1)
            {
                AddWarningMessage(_args[0]);
                return "";
            }
            return "AddWarningMessage requires exactly 1 argument";
        }
        public void AddWarningMessage(string _message)
        {
            if (!string.IsNullOrWhiteSpace(_message))
            {
                AddMessage(_message, DisplayableMessage.eLevel.Level_Warning);
            }
        }
        private string AddErrorMessage(List<string> _args)
        {
            if (_args.Count == 1)
            {
                AddErrorMessage(_args[0]);
                return "";
            }
            return "AddErrorMessage requires exactly 1 argument";
        }
        public void AddErrorMessage(string _message)
        {
            if (!string.IsNullOrWhiteSpace(_message))
            {
                AddMessage(_message, DisplayableMessage.eLevel.Level_Error);
            }
        }

        public void ActivateCommand()
        {
            if (string.IsNullOrEmpty(m_strCurrentCommand))
                return;

            var commandManager = ManagerUtils.GetCommandManager();
            if(commandManager)
            {
                List<string> components = m_strCurrentCommand.Split(' ').ToList();
                components = components.Where(c => !string.IsNullOrWhiteSpace(c)).ToList();

                if (components.Any())
                {
                    if (components.Count > 1)
                    {
                        List<string> args = components.GetRange(1, components.Count - 1);
                        AddInfoMessage(commandManager.ActivateCommand(components[0], args));
                    }
                    else
                    {
                        AddInfoMessage(commandManager.ActivateCommand(components[0]));
                    }

                    m_commandQueue.Enqueue(m_strCurrentCommand);

                    if(m_commandQueue.Count > MAX_QUEUE_SIZE)
                    {
                        m_commandQueue.Dequeue();
                    }
                    m_iCurrentQueueIndex = m_commandQueue.Count;

                }


                m_strCurrentCommand = "";
            }

            m_bScrollToBottomNextFrame = m_bAutoScrollToBottom;
        }

        private string GetPreviousMessage()
        {
            if(--m_iCurrentQueueIndex < 0)
            {
                m_iCurrentQueueIndex = 0;
            }

            if(m_iCurrentQueueIndex >= m_commandQueue.Count)
            {
                return "";
            }
            return m_commandQueue.ElementAt(m_iCurrentQueueIndex);
        }
        private string GetNextMessage()
        {
            ++m_iCurrentQueueIndex;
            if (m_iCurrentQueueIndex >= m_commandQueue.Count)
            {
                m_iCurrentQueueIndex = m_commandQueue.Count;
                return "";
            }
            return m_commandQueue.ElementAt(m_iCurrentQueueIndex);
        }
    }
}