﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.User
{
    public class OnlineProfile : ScriptableObject
    {
        private string m_strPlayFabId;
        public string PlayFabId { get => m_strPlayFabId; }
        public void SetPlayFabId(string _strId)
        {
            m_strPlayFabId = _strId;
        }

        private List<string> m_tags;
        public List<string> Tags { get => m_tags; }
        public void SetTags(List<string> _tags)
        {
            m_tags = _tags;
        }
    }
}