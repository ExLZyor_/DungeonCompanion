﻿using Assets.Scripts.MainCode.PlayFab.Model;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.PlayFab.Client.Profile
{
    public class PlayFabGetPlayerTagsRequest : PlayFabRequest
    {
        private string m_strPlayFabId;
        public void SetPlayFabId(string _strId)
        {
            m_strPlayFabId = _strId;
        }


        private List<string> m_resultTags;
        public List<string> Tags { get => m_resultTags; }

        public PlayFabGetPlayerTagsRequest()
        {
            m_resultTags = new List<string>();
        }

        public override void Start()
        {
            var request = new GetPlayerTagsRequest
            {
                AuthenticationContext = Globals.ManagerUtils.GetPlayFabAuthenticationContext(),
                PlayFabId = m_strPlayFabId
            };

            PlayFabClientAPI.GetPlayerTags(request, _OnGetPlayerTagsSuccess, _OnGetPlayerTagsFailure);
        }
        private void _OnGetPlayerTagsSuccess(GetPlayerTagsResult _result)
        {
            m_resultTags = _result.Tags;

            StringBuilder builder = new StringBuilder(string.Format("Request {0} suceeded. Tags ({1}): ", m_strId, m_resultTags.Count));
            foreach(var tag in m_resultTags)
            {
                builder.Append(string.Format("'{0}'", tag));
            }

            m_service.AddLogMessage(builder.ToString());
            SetState(eRequestState.RequestState_Succeeded);
        }
        private void _OnGetPlayerTagsFailure(PlayFabError error)
        {
            m_service.AddLogMessage(string.Format("Request {0} failed. PlayFab Error : {1}", m_strId, error.ErrorMessage));
            m_error = error;
            SetState(eRequestState.RequestState_Failed);
        }
    }
}