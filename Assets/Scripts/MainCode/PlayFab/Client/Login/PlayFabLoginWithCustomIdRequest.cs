﻿using Assets.Scripts.MainCode.PlayFab.Model;
using PlayFab;
using PlayFab.ClientModels;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.PlayFab.Client.Login
{
    public class PlayFabLoginWithCustomIdRequest : PlayFabRequest
    {
        private string m_strCustomId;
        public string CustomId { get => m_strId; }
        public void SetCustomId(string _strCustomId)
        {
            m_strCustomId = _strCustomId;
        }

        private bool m_bCreateAccount;
        public bool CreateAccount { get => m_bCreateAccount; }
        public void SetCreateAccount(bool _bState)
        {
            m_bCreateAccount = _bState;
        }

        #region Results
        private string m_strPlayFabId;
        public string PlayFabId { get => m_strPlayFabId; }

        private PlayFabAuthenticationContext m_authenticationContext;
        public PlayFabAuthenticationContext AuthenticationContext { get => m_authenticationContext; }

        #endregion


        public override void Start()
        {
            var request = new LoginWithCustomIDRequest
            {
                CustomId = m_strCustomId,
                CreateAccount = CreateAccount
            };

            PlayFabClientAPI.LoginWithCustomID(request, _OnLoginWithCustomIdSuccess, _OnLoginWithCustomIdFailure);
        }

        private void _OnLoginWithCustomIdSuccess(LoginResult _result)
        {
            m_strPlayFabId = _result.PlayFabId;
            m_authenticationContext = _result.AuthenticationContext;
            m_service.AddLogMessage(string.Format("Request {0} suceeded. PlayFab Id : {1}", m_strId, m_strPlayFabId));
            SetState(eRequestState.RequestState_Succeeded);
        }
        private void _OnLoginWithCustomIdFailure(PlayFabError error)
        {
            m_service.AddLogMessage(string.Format("Request {0} failed. PlayFab Error : {1}", m_strId, error.ErrorMessage));
            m_error = error;
            SetState(eRequestState.RequestState_Failed);
        }
    }
}