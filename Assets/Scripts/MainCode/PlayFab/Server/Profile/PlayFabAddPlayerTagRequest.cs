#if ENABLE_PLAYFABSERVER_API
using Assets.Scripts.MainCode.PlayFab.Model;
using PlayFab;
using PlayFab.ServerModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MainCode.PlayFab.Server.Profile
{
    public class PlayFabAddPlayerTagRequest : PlayFabRequest
    {
        private string m_strPlayFabId;
        public void SetPlayFabId(string _strId)
        {
            m_strPlayFabId = _strId;
        }


        private string m_strTag;
        public void SetTag(string _tag)
        {
            m_strTag = _tag;
        }


        public PlayFabAddPlayerTagRequest()
        {

        }

        public override void Start()
        {
            var request = new AddPlayerTagRequest
            {

                AuthenticationContext = Globals.ManagerUtils.GetPlayFabAuthenticationContext(),
                PlayFabId = m_strPlayFabId,
                TagName = m_strTag
            };

            PlayFabServerAPI.AddPlayerTag(request, _OnAddPlayerTagSuccess, _OnAddPlayerTagsFailure);
        }
        private void _OnAddPlayerTagSuccess(AddPlayerTagResult _result)
        {
            m_service.AddLogMessage(string.Format("Request {0} succeeded.", m_strId));
            SetState(eRequestState.RequestState_Succeeded);
        }
        private void _OnAddPlayerTagsFailure(PlayFabError error)
        {
            m_service.AddLogMessage(string.Format("Request {0} failed. PlayFab Error : {1}", m_strId, error.ErrorMessage));
            m_error = error;
            SetState(eRequestState.RequestState_Failed);
        }
    }
}
#endif
