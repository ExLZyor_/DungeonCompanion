using Assets.Scripts.MainCode.Globals;
using Assets.Scripts.MainCode.PlayFab.Client.Login;
using Assets.Scripts.MainCode.PlayFab.Model;
using ImGuiNET;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MainCode.PlayFab.Services.Login
{
    public class PlayFabLoginService : PlayFabService
    {
        private PlayFabLoginWithCustomIdRequest m_testLoginWithCustomId;
        private string m_strCustomId;

        public PlayFabLoginService(string _strName) : base(_strName)
        {

        }
        public override void InitService()
        {
            m_testLoginWithCustomId = new PlayFabLoginWithCustomIdRequest();
            m_strCustomId = "";
        }

        public PlayFabLoginWithCustomIdRequest RequestLoginWithCustomId(string _customId, bool _bCanCreateAccount)
        {
            PlayFabLoginWithCustomIdRequest request = new PlayFabLoginWithCustomIdRequest();
            request.SetCustomId(_customId);

            string strRequestId = string.Format("loginwithcustomid-{0}", _customId);
            request.SetId(strRequestId);
            request.SetCreateAccount(_bCanCreateAccount);
            request.SetService(this);

            request.Start();

            return request;
        }

        public override void DrawDebug()
        {
            ImGui.BeginGroup();
            base.DrawDebug();
            if(ImGui.CollapsingHeader("TESTS"))
            {
                ImGui.Indent();
                if (ImGui.CollapsingHeader("Test LoginWithCustomId"))
                {
                    ImGui.Text("Custom Id :");
                    ImGui.InputText("", ref m_strCustomId, 6);

                    if(ImGui.Button("Test"))
                    {
                        if(!m_testLoginWithCustomId.IsStarted() || m_testLoginWithCustomId.IsCompleted())
                        {
                            m_testLoginWithCustomId = RequestLoginWithCustomId(m_strCustomId, false);
                        }
                    }

                    if (m_testLoginWithCustomId.IsSuceeded())
                    {
                        ImGui.TextColored(Color.green, string.Format("Request success ! PlayFab Id : {0}", m_testLoginWithCustomId.PlayFabId));
                    }
                    else if (m_testLoginWithCustomId.IsFailed())
                    {
                        ImGui.TextColored(Color.red, string.Format("Request failed... PlayFab Error : {0}", m_testLoginWithCustomId.Error.ErrorMessage));
                    }
                }
                ImGui.Unindent();
            }
            ImGui.EndGroup();
        }

    }
}
