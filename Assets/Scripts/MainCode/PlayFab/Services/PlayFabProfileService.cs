﻿using Assets.Scripts.MainCode.PlayFab.Client.Profile;
using Assets.Scripts.MainCode.PlayFab.Model;
using Assets.Scripts.MainCode.PlayFab.Server.Profile;
using ImGuiNET;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.PlayFab.Services.Profile
{
    public class PlayFabProfileService : PlayFabService
    {
        private PlayFabGetPlayerTagsRequest m_testGetPlayerTags;
        private string m_strPlayFabId;

#if ENABLE_PLAYFABSERVER_API
        private PlayFabAddPlayerTagRequest m_testAddPlayerTag;
        private string m_strPlayFabIdTag;
        private string m_tag;
#endif



        public PlayFabProfileService(string _strName) : base(_strName)
        {

        }
        public override void InitService()
        {
            m_testGetPlayerTags = new PlayFabGetPlayerTagsRequest();
            m_strPlayFabId = "";

            m_testAddPlayerTag = new PlayFabAddPlayerTagRequest();
            m_tag = "";
            m_strPlayFabIdTag = "";
        }

        public PlayFabGetPlayerTagsRequest RequestGetPlayerTags(string _playFabId)
        {
            PlayFabGetPlayerTagsRequest request = new PlayFabGetPlayerTagsRequest();
            string strRequestId = string.Format("getplayertags-{0}", _playFabId);
            request.SetId(strRequestId);
            request.SetPlayFabId(_playFabId);
            request.SetService(this);

            request.Start();

            return request;
        }

#if ENABLE_PLAYFABSERVER_API
        public PlayFabAddPlayerTagRequest RequestAddPlayerTag(string _playFabId, string _tag)
        {
            PlayFabAddPlayerTagRequest request = new PlayFabAddPlayerTagRequest();
            string strRequestId = string.Format("addplayertag-{0}", _playFabId);
            request.SetId(strRequestId);
            request.SetPlayFabId(_playFabId);
            request.SetTag(_tag);
            request.SetService(this);

            request.Start();

            return request;
        }
#endif

        public override void DrawDebug()
        {
            ImGui.BeginGroup();
            base.DrawDebug();
            if (ImGui.CollapsingHeader("TESTS"))
            {
                ImGui.Indent();
                if (ImGui.CollapsingHeader("Test GetPlayerTags"))
                {
                    ImGui.Text("PlayFab Id :");
                    ImGui.InputText("", ref m_strPlayFabId, 16);

                    if (ImGui.Button("Test"))
                    {
                        if (!m_testGetPlayerTags.IsStarted() || m_testGetPlayerTags.IsCompleted())
                        {
                            m_testGetPlayerTags = RequestGetPlayerTags(m_strPlayFabId);
                        }
                    }

                    if (m_testGetPlayerTags.IsSuceeded())
                    {
                        ImGui.TextColored(Color.green, string.Format("Request success ! Tags ({0}):", m_testGetPlayerTags.Tags.Count));
                        foreach(var tag in m_testGetPlayerTags.Tags)
                        {
                            ImGui.TextColored(Color.green, string.Format("- {0}", tag));
                        }
                    }
                    else if (m_testGetPlayerTags.IsFailed())
                    {
                        ImGui.TextColored(Color.red, string.Format("Request failed... PlayFab Error : {0}", m_testGetPlayerTags.Error.ErrorMessage));
                    }
                }
#if ENABLE_PLAYFABSERVER_API
                if (ImGui.CollapsingHeader("Test AddPlayerTag"))
                {
                    ImGui.Text("PlayFab Id :");
                    ImGui.InputText("P", ref m_strPlayFabIdTag, 16);
                    ImGui.Text("Tag :");
                    ImGui.InputText("Tag", ref m_tag, 24);

                    if (ImGui.Button("Test"))
                    {
                        if (!m_testAddPlayerTag.IsStarted() || m_testAddPlayerTag.IsCompleted())
                        {
                            m_testAddPlayerTag = RequestAddPlayerTag(m_strPlayFabIdTag, m_tag);
                        }
                    }

                    if (m_testAddPlayerTag.IsSuceeded())
                    {
                        ImGui.TextColored(Color.green, "Request success !");
                    }
                    else if (m_testGetPlayerTags.IsFailed())
                    {
                        ImGui.TextColored(Color.red, string.Format("Request failed... PlayFab Error : {0}", m_testAddPlayerTag.Error.ErrorMessage));
                    }
                }
#endif
                ImGui.Unindent();
            }
            ImGui.EndGroup();
        }
    }
}