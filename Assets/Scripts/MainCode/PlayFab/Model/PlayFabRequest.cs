﻿using PlayFab;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.PlayFab.Model
{
    public abstract class PlayFabRequest
    {
        public enum eRequestState
        {
            RequestState_Started,
            RequestState_Succeeded,
            RequestState_Failed,
            RequestState_Idle
        }

        protected eRequestState m_eState;
        public eRequestState State { get => m_eState; }
        public void SetState(eRequestState _eState)
        {
            m_eState = _eState;
        }

        protected string m_strId;
        public string Id { get => m_strId; }
        public void SetId(string _strId)
        {
            m_strId = _strId;
        }


        protected PlayFabService m_service;
        public void SetService(PlayFabService _service)
        {
            m_service = _service;
        }

        protected PlayFabError m_error;
        public PlayFabError Error { get => m_error; }

        public PlayFabRequest()
        {
            Reset();
        }

        public virtual void Reset()
        {
            SetState(eRequestState.RequestState_Idle);
            m_error = null;
        }

        public virtual void Start()
        {
            SetState(eRequestState.RequestState_Started);
        }

        public bool IsSuceeded()
        {
            return m_eState == eRequestState.RequestState_Succeeded;
        }
        public bool IsFailed()
        {
            return m_eState == eRequestState.RequestState_Failed;
        }
        public bool IsStarted()
        {
            return m_eState != eRequestState.RequestState_Idle;
        }
        public bool IsCompleted()
        {
            return (m_eState == eRequestState.RequestState_Succeeded || m_eState == eRequestState.RequestState_Failed);
        }
    }
}