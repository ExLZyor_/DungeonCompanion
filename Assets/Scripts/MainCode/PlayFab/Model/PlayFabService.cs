﻿using ImGuiNET;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.MainCode.PlayFab.Model
{
    public abstract class PlayFabService
    {
        protected Dictionary<string, PlayFabRequest> m_requests;
        protected string m_strDebugName;
        public string DebugName { get => m_strDebugName; }
        private List<string> m_logs;

        protected Rect m_currentWindowRect;

        public PlayFabService(string _strName)
        {
            m_strDebugName = _strName;
            m_logs = new List<string>();
        }

        public abstract void InitService();

        public void AddRequest(string _strId, PlayFabRequest _request)
        {
            m_requests.Add(_strId, _request);
        }

        public void AddLogMessage(string _strLog)
        {
            m_logs.Add(_strLog);
        }

        public virtual void DrawDebug()
        {
            if(ImGui.CollapsingHeader("LOGS"))
            {
                foreach(var log in m_logs)
                {
                    ImGui.Text(log);
                }
            }
        }
    }
}